//
//  NovoJogo.swift
//  Emoji_01
//
//  Created by Eliza Maria Porto de Carvalho on 15/08/2018.
//  Copyright © 2018 Academy. All rights reserved.
//

import Foundation

class Emoji{
    
    let emoji: String
    var descricao: String
    
    init(emoji:String, descricao:String) {
        
        self.emoji = emoji
        self.descricao = descricao

    }
    
    func getEmoji() -> String{
        
        return self.emoji
    }
    
    func getDescricao() -> String{
        return self.descricao
    }
    
    func setDescricao(descricao: String){
        self.descricao = descricao
    }
    
    
}
