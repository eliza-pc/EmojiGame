//
//  DescriptionViewController.swift
//  Emoji_01
//
//  Created by Eliza Maria Porto de Carvalho on 09/08/2018.
//  Copyright © 2018 Academy. All rights reserved.
//
import UIKit

class DescriptionViewController: UIViewController {

    @IBOutlet weak var descripitionEMJ: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    var emoji = ["💜":"Coracão Roxo", "😂": "Rosto chorando de rir", "🔮": "Bola de Cristal","😵" : "Rosto atordoado", "🦄": "Unicórnio","🐧":"Pinguim","🥔":"Batata","🧛‍♀️":"Vampira","😭":"Rosto chorando aos berros","💁‍♀️":"Mulher do b o","🧚‍♀️":"Fada","⚠️":"Aviso","🧜‍♀️":"Sereia",]
    
    @IBAction func CoracaoRoxo(_ sender: Any) {
        descripitionEMJ.text = emoji["💜"]
    }
    @IBAction func RostoChorandoDeRir(_ sender: Any) {
         descripitionEMJ.text = emoji["😂"]
    }
    @IBAction func BolaDeCristal(_ sender: Any) {
         descripitionEMJ.text = emoji["🔮"]
    }
    @IBAction func RostoAtordoado(_ sender: Any) {
         descripitionEMJ.text = emoji["😵"]
    }
    @IBAction func Unicornio(_ sender: Any) {
         descripitionEMJ.text = emoji["🦄"]
    }
    @IBAction func Penguim(_ sender: Any) {
         descripitionEMJ.text = emoji["🐧"]
    }
    @IBAction func Batata(_ sender: Any) {
         descripitionEMJ.text = emoji["🥔"]
    }
    @IBAction func Vampira(_ sender: Any) {
         descripitionEMJ.text = emoji["🧛‍♀️"]
    }
    @IBAction func RostoChorandoAosBerros(_ sender: Any) {
         descripitionEMJ.text = emoji["😭"]
    }
    @IBAction func MulherDoBalcaoDeInformacoes(_ sender: Any) {
         descripitionEMJ.text = emoji["💁‍♀️"]
    }
    @IBAction func Elfa(_ sender: Any) {
         descripitionEMJ.text = emoji["🧝‍♀️"]
    }
    @IBAction func MulherZombie(_ sender: Any) {
         descripitionEMJ.text = emoji["🧟‍♀️"]
    }
    @IBAction func Panqueca(_ sender: Any) {
         descripitionEMJ.text = emoji["🥞"]
    }
    @IBAction func ArcoIris(_ sender: Any) {
         descripitionEMJ.text = emoji["🌈"]
    }
    @IBAction func Colisao(_ sender: Any) {
         descripitionEMJ.text = emoji["💥"]
    }
    @IBAction func HomemGenioDaLampada(_ sender: Any) {
         descripitionEMJ.text = emoji["🧞‍♂️"]
    }
    @IBAction func LuaNovaComRosto(_ sender: Any) {
         descripitionEMJ.text = emoji["🌚"]
    }
    @IBAction func Fada(_ sender: Any) {
         descripitionEMJ.text = emoji["🧚‍♀️"]
    }
    @IBAction func Alerta(_ sender: Any) {
         descripitionEMJ.text = emoji["⚠️"]
    }
    @IBAction func Sereia(_ sender: Any) {
         descripitionEMJ.text = emoji["🧜‍♀️"]
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
