//
//  GameViewController.swift
//  Emoji_01
//
//  Created by Eliza Maria Porto de Carvalho on 09/08/2018.
//  Copyright © 2018 Academy. All rights reserved.
//

import UIKit

class GameViewController: UIViewController {

    @IBOutlet weak var descripitionEMJ: UILabel!
    @IBOutlet weak var resposta: UILabel!
    var descricao: String = " "
    //test
    //test
    
    func randomDescricao(){
        
        var emojis = ["💜":"Coracao Roxo", "😂": "Rosto chorando de rir", "🔮": "Bola de cristal","😵" : "Rosto atordoado", "🦄": "Unicórnio","🐧":"Pinguim","🥔":"Batata","🧛‍♀️":"Vampira","😭":"Rosto chorando aos berros","💁‍♀️":"Mulher do balcao de informacoes","🧝‍♀️":"Elfa","🧟‍♀️":"Mulher Zombie","🥞":"Paqueca","🌈":"Arco-Íris","💥":"Colisao","🧞‍♂️":"Homem Genio da Lampada","🌚":"Lua Nova com Rosto","🧚‍♀️":"Fada","⚠️":"Aviso","🧜‍♀️":"Sereia",]
    
        let descricoes = Array(emojis.values)
        let randomEmoji = Int(arc4random_uniform(UInt32(emojis.count)))
        
        descripitionEMJ.text = descricoes[randomEmoji]
        
    }

    @IBAction func START(_ sender: Any) {
        randomDescricao()
    }
    
    @IBAction func CoracaoRoxo(_ sender: Any) {
      descricao = "Coracao Roxo"
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
    }
    
    @IBAction func RostoChorandoDeRir(_ sender: Any) {
        descricao = "Rosto chorando de rir"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func BolaDeCristal(_ sender: Any) {
        descricao = "Bola de cristal"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func RostoAtordoado(_ sender: Any) {
        descricao = "Rosto atordoado"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func Unicornio(_ sender: Any) {
        descricao = "Unicornio"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func Penguim(_ sender: Any) {
        descricao = "Peguim"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func Batata(_ sender: Any) {
        descricao = "Batata"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func Vampira(_ sender: Any) {
        descricao = "Vampira"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func RostoChorandoAosBerros(_ sender: Any) {
        descricao = "Rosto chorando aos berros"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func MulherDoBalcaoDeInformacoes(_ sender: Any) {
        descricao = "Mulher do balco de informacoes"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func Elfa(_ sender: Any) {
        descricao = "Elfa"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
        
    }
    @IBAction func MulherZombie(_ sender: Any) {
        descricao = "Mulher Zombie"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func Panqueca(_ sender: Any) {
        descricao = "Panqueca"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func ArcoIris(_ sender: Any) {
        descricao = "Arco-Íris"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func Colisao(_ sender: Any) {
        descricao = "Colisao"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func HomemGenioDaLampada(_ sender: Any) {
        descricao = "Homem Genio da Lampada"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func LuaNovaComRosto(_ sender: Any) {
        descricao = "Lua Nova com Rosto"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func Fada(_ sender: Any) {
        descricao = "Fada"
        
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
        
    }
    @IBAction func Alerta(_ sender: Any) {
        descricao = "Alerta"
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
    }
    @IBAction func Sereia(_ sender: Any) {
        descricao = "Sereia"
        if descricao == descripitionEMJ.text{
            resposta.text = "Acertou!"
        }else{
            resposta.text = "Errou!"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
