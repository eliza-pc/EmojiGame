//
//  3TelaViewController.swift
//  Emoji_01
//
//  Created by Eliza Maria Porto de Carvalho on 15/08/2018.
//  Copyright © 2018 Academy. All rights reserved.
//

import UIKit

class _TelaViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var arrayEmoji: [Emoji] = [Emoji.init(emoji: "🐧", descricao: "pinguim"),
                               Emoji.init(emoji: "🧜‍♀️", descricao: "sereia"),
                               Emoji.init(emoji: "🧚‍♀️", descricao: "Fada"),
                               Emoji.init(emoji: "🧟‍♀️", descricao: "Mulher Zombie"),
                               Emoji.init(emoji: "🧝‍♀️", descricao: "Elfa"),
                               Emoji.init(emoji: "🥞", descricao: "Panqueca"),
                               Emoji.init(emoji: "💜", descricao: "Coracao Roxo"),
                               Emoji.init(emoji: "😂", descricao: "Rosto chorando de rir"),
                               Emoji.init(emoji: "🔮", descricao: "Bola de cristal"),
                               Emoji.init(emoji: "😵", descricao: "Rosto atordoado"),
                               Emoji.init(emoji: "🦄", descricao: "Unicórnio") ,
                               Emoji.init(emoji: "🐧", descricao: "Pinguim"),
                               Emoji.init(emoji: "🥔", descricao: "Batata"),
                               Emoji.init(emoji: "🧛‍♀️", descricao: "Vampira"),
                               Emoji.init(emoji: "😭", descricao: "Rosto chorando aos berros"),
                               Emoji.init(emoji: "💁‍♀️", descricao: "Mulher do balcao de informacoes"),
                               Emoji.init(emoji: "🧝‍♀️", descricao: "Elfa"),
                               Emoji.init(emoji: "🧟‍♀️", descricao: "Mulher Zombie"),
                               Emoji.init(emoji: "🥞", descricao: "Paqueca"),
                               Emoji.init(emoji: "🌈", descricao: "Arco-Íris"),]
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
